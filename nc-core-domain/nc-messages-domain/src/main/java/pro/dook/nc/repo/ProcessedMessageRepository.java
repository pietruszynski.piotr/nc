package pro.dook.nc.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pro.dook.nc.model.ProcessedMessage;

public interface ProcessedMessageRepository extends JpaRepository<ProcessedMessage, Long> {
}

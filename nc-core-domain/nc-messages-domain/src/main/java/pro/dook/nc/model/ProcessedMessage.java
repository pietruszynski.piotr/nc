package pro.dook.nc.model;

import javax.persistence.*;

import java.time.LocalDateTime;

import static validator.Validator.requireNonEmpty;
import static validator.Validator.requireNonNull;

@Entity
@Table(name = "processed_messages")
public class ProcessedMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 1024)
    private String message;

    @Column
    private LocalDateTime receivedDate;

    public ProcessedMessage(String message, LocalDateTime receivedDate) {
        this.message = requireNonEmpty(message, "message");
        this.receivedDate = requireNonNull(receivedDate, "received date");
    }


    public long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getReceivedDate() {
        return receivedDate;
    }
}
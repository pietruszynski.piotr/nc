package pro.dook.nc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NCServer {

    public static void main(String[] args) {
        SpringApplication.run(NCServer.class, args);
    }

}

package pro.dook.nc;

import static validator.Validator.requireNonEmpty;

public class ReceivedMessage {

    private final String message;

    public ReceivedMessage(String message) {
        this.message = requireNonEmpty(message, "message");
    }

    public String getMessage() {
        return message;
    }

}

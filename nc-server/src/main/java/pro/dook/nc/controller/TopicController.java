package pro.dook.nc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import pro.dook.nc.ReceivedMessage;
import pro.dook.nc.TopicService;
import pro.dook.nc.model.ProcessedMessage;

@Controller
public class TopicController {

    @Autowired
    private TopicService topicService;

    @MessageMapping("/message")
    @SendTo("/topic")
    public ProcessedMessage process(ReceivedMessage message) {
        return topicService.process(message);
    }

}

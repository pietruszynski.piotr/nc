package pro.dook.nc;

import org.springframework.stereotype.Service;
import pro.dook.nc.model.ProcessedMessage;
import pro.dook.nc.repo.ProcessedMessageRepository;

import java.time.LocalDateTime;

import static validator.Validator.requireNonNull;

@Service
public class TopicService {

    private final ProcessedMessageRepository messageRepository;

    public TopicService(ProcessedMessageRepository messageRepository) {
        this.messageRepository = requireNonNull(messageRepository, "message repository");
    }

    public ProcessedMessage process(ReceivedMessage message) {
        LocalDateTime now = LocalDateTime.now();
        String content = message.getMessage();
        ProcessedMessage processedMessage = new ProcessedMessage(content, now);
        messageRepository.save(processedMessage);
        return processedMessage;
    }

}

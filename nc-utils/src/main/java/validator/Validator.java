package validator;

public class Validator {

    private Validator() {

    }

    public static <T> T requireNonNull(T object) {
        if (object == null) throw new IllegalArgumentException();
        return object;
    }

    public static <T> T requireNonNull(T object, String name) {
        if (object == null) throw new IllegalArgumentException(name + " can't be null.");
        return object;
    }

    public static String requireNonEmpty(String str) {
        if (str == null || str.isEmpty()) throw new IllegalArgumentException();
        return str;
    }

    public static String requireNonEmpty(String str, String name) {
        if (str == null || str.isEmpty()) throw new IllegalArgumentException(name + " can't be empty.");
        return str;
    }

    public static int requireNonNegative(int number) {
        if (number < 0) throw new IllegalArgumentException();
        return number;
    }

    public static int requireNonNegative(int number, String name) {
        if (number < 0) throw new IllegalArgumentException(name + " can't be negative.");
        return number;
    }

    public static long requireNonNegative(long number) {
        if (number < 0) throw new IllegalArgumentException();
        return number;
    }

    public static long requireNonNegative(long number, String name) {
        if (number < 0) throw new IllegalArgumentException(name + " can't be negative.");
        return number;
    }

}
